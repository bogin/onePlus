/**
* WooItemStorageController
*
* @description :: Server-side logic for managing wooitemstorages
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/
var uniqid = require('uniqid');

module.exports = {
    delete: function (req, res) {
        return res.json({
            todo: 'delete() is not implemented yet!'
        });
    },


    update: function (req, res) {
        return res.json({
            todo: 'update() is not implemented yet!'
        });
    },

    showHome: function(req, res){
      var name = "",
          description = "",
          imgSrc = "",
          numOfDeals = "",
          link = "";

      res.view('homepage', {


          /*** meta begin ***/
          pageTitle: name + ' - WeBuyTwo',
          pageDescription: description,
          shareTitle: name + ' - WeBuyTwo',
          shareDescription: description,
          pageUrl: link,
          shareImage: imgSrc,
          /*** meta end ***/
          imgSrc: imgSrc,
          productName: name,
          facebook: "",
          twitter: "",
          email: "",
          whatsapp: "",
          productDescription: description,
          productRemainingDeals: numOfDeals
      });
    },
    showProductStepTwo: function(req, res){
        var productId = req.param('productId'),
            mainUserId = req.param('mainUserId'),
            shopName = req.param('shopName');

        WooItemStorage.query('SELECT * FROM `wooitemstorage` WHERE id=' + productId + '', [], function(err, rawResult) {
            if (err) {
                return res.serverError(err);
            }

            var name = rawResult[0]['name'],
                description = rawResult[0]['description'],
                imgSrc = rawResult[0]['imgSrc'],
                numOfDeals = rawResult[0]['numOfDeals'],
                link = "http://webuytwo.com:49158/userTable/" + shopName + "/" + productId + "/" + mainUserId;

            res.view('three', {

                /*** meta begin ***/
                pageTitle: name + ' - WeBuyTwo',
                pageDescription: description,
                shareTitle: name + ' - WeBuyTwo',
                shareDescription: description,
                pageUrl: link,
                shareImage: imgSrc,
                /*** meta end ***/
                imgSrc: imgSrc,
                productName: name,
                facebook: "http://www.facebook.com/sharer.php?u=" + ((link.length) ? link : window.location.href.split("/")[1]) + "?picture="+imgSrc,
                twitter: "https://twitter.com/share?text="  + "&amp;url=" + ((link.length) ? link : window.location.href.split("?")[0]),
                email: "mailto:?subject="+name + ' - WeBuyTwo'+ "&amp;" + "&amp;body="+link,
                // Subject="  + "&amp;Body=<a href='" + ((link.length) ? link : window.location.href.split("?")[0]) + "'>" + name + ' - WeBuyTwo' + " - "  + "</a>",
                whatsapp: "whatsapp://send?text=" + link,
                productDescription: description,
                productRemainingDeals: numOfDeals
            });
        });
    },


    showProductStepOne: function(req, res) {
        res.redirect(req.baseUrl + req.url + "/" + uniqid());
    },


    post: function (req, res) {
        return res.json({
            todo: 'post() is not implemented yet!'
        });
    }
};
