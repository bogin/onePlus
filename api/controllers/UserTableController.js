/**
 * UserTableController
 *
 * @description :: Server-side logic for managing usertables
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var uniqid = require('uniqid');


module.exports = {

  showProductToJoinerWithCookie: function (req, res) {

    var name = rawResult[0]['name'],
        description = rawResult[0]['description'],
        imgSrc = rawResult[0]['imgSrc'],
        numOfDeals = rawResult[0]['numOfDeals'],
        link = "http://webuytwo.com:49158/userTable/" + shopName + "/" + productId + "/" + mainUserId;
    res.view('joinerFirest', {

        /*** meta begin ***/
        pageTitle: name + ' - WeBuyTwo',
        pageDescription: description,
        shareTitle: name + ' - WeBuyTwo',
        shareDescription: description,
        pageUrl: link,
        shareImage: imgSrc,
        /*** meta end ***/
        imgSrc: imgSrc,
        productName: name,
        facebook: "http://www.facebook.com/sharer.php?u=" + ((link.length) ? link : window.location.href.split("/")[1]) + "?picture="+imgSrc,
        twitter: "https://twitter.com/share?text="  + "&amp;url=" + ((link.length) ? link : window.location.href.split("?")[0]),
        email: "mailto:?subject="+name + ' - WeBuyTwo'+ "&amp;" + "&amp;body="+link,
        // Subject="  + "&amp;Body=<a href='" + ((link.length) ? link : window.location.href.split("?")[0]) + "'>" + name + ' - WeBuyTwo' + " - "  + "</a>",
        whatsapp: "whatsapp://send?text=" + link,
        productDescription: description,
        productRemainingDeals: numOfDeals
    });
  },


  showProductToJoiner: function (req, res) {
    var userId = uniqid();
    var itemId = req.param("productId");
    var shopName = req.param("shopName");
    var mainUserId = req.param("mainUserId");

    UserTable.query('UPDATE `usertable` SET `views`= `views` + 1 WHERE `itemId` ='+itemId, [] ,function(err, rawResult) {
      if (err) { return res.serverError(err); }
    });
    res.redirect(req.baseUrl + req.url +"/"+userId);
  },
};
