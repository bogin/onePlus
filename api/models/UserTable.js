/**
 * UserTable.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id : { type: 'string' },
    itemId : { type: 'string'},
    store : { type: 'string'},
    isMainUser : { type: 'boolean'},
    isInitiator : { type: 'boolean'},
    isCradintIn : { type: 'boolean'},
    views : { type: 'int' },
  }
};

