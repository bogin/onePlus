/**
 * WooItemStorage.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {


    id : { type: 'int' },

    shopId : { type: 'int' },

    description : { type: 'text' },

    imgSrc : { type: 'text' },

    kindOfDeal : { type: 'text' },

    name : { type: 'text' },

    numOfDeals : { type: 'text' },

    price : { type: 'text' },

    shopName : { type: 'text' },
    views : { type: 'int' },
    couponId : { type: 'int' },

  }
};
