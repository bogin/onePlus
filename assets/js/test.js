(($) => {
    const getTextsFromPage = ($currentElement, texts) => {
        let currentText = $currentElement
                .clone()    //clone the element
                .children() //select all the children
                .remove()   //remove all the children
                .end()  //again go back to selected element
                .text();

        // if the current element is not a script tag
        // and if the current text has characters that are not whitespaces or line breaks
        if (!$currentElement.is("script") && /\S/.test(currentText)) {
            texts.push(currentText.trim());
        }

        // if the current element has child elements
        if ($currentElement.children().length) {
            // iterate the current element's child elements
            $currentElement.children().each(function() {
                // add their content to the texts object
                texts = getTextsFromPage($(this), texts);
            });
        }
        // after all iterations and recursions are complete, return all the texts.
        return texts;
    },

    sendToServer = () => {
        //window.youTexts
    },

    init = () => {
        window.youTexts = getTextsFromPage($("body"), []);
    };

    $(() => {
        init();
    });
})(jQuery);
